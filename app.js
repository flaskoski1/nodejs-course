const http = require("http");
const path = require("path");
const bodyParser = require("body-parser");
//const { engine } = require("express-handlebars");

const express = require("express");
const db = require("./util/database");
const sequelize = require("./util/database");

const Product = require("./models/product");
const User = require("./models/user");
const CartItem = require("./models/cart-item");
const Cart = require("./models/cart");
const OrderItem = require("./models/order-item");
const Order = require("./models/order");

const app = express();

// app.engine(
//   "hbs",
//   engine({
//     extname: "handlebars",
//     layoutsDir: "views/layouts",
//     defaultLayout: "main-layout",
//   })
// );

app.set("view engine", "ejs");
app.set("views", "views");

const adminRoutes = require("./routes/admin");
const shopRoutes = require("./routes/shop");

const errorController = require("./controllers/error");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "public")));

app.use((req, res, next) => {
  User.findByPk(1)
    .then((user) => {
      req.user = user;
      next();
    })
    .catch((err) => console.log(err));
});

app.use("/admin", adminRoutes);
app.use(shopRoutes);

app.use(errorController.get404);

Product.belongsTo(User, { constraints: true, onDelete: "CASCADE" });
User.hasMany(Product);
User.hasOne(Cart);
Cart.belongsTo(User);
Cart.belongsToMany(Product, { through: CartItem });
Product.belongsToMany(Cart, { through: CartItem });
Order.belongsTo(User);
User.hasMany(Order);
Order.belongsToMany(Product, { through: OrderItem });

sequelize
  //.sync({ force: true })
  .sync()
  .then((result) => {
    return User.findByPk(1);
    //console.log(result);
  })
  .then((user) => {
    if (!user) {
      return User.create({ name: "Filip", email: "flaskoski@gmail.com" });
    }
    return user;
  })
  .then((user) => {
    //console.log(user);
    return user.createCart();
  })
  .then((cart) => {
    app.listen(3000);
  })
  .catch((err) => {
    console.log(err);
  });

// app.use((req, res, next) => {
//   console.log("in the middleware");
//   next(); // allows the request to continue to the next middleware in line
// });

// app.use("/", (req, res, next) => {
//   // console.log("this always runs");
//   next();
// });
// app.use("/add-product", (req, res, next) => {
//   //console.log("in another the middleware");
//   res.send(
//     "<form action='/product' method='POST'><input type='text' name='title'><button type='submit'>Add Product</button></form>"
//   );
// });

// app.post("/product", (req, res, next) => {
//   console.log(req.body);
//   res.redirect("/");
// });

// app.use("/", (req, res, next) => {
//   //  console.log("in another the middleware");
//   res.send("<h1>Hello from express</h1>");
// });

// const server = http.createServer(app);
// server.listen(3000);
